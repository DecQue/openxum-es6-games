require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('Coordinates', () => {
  test('to string', () => {
    const coordinates = new OpenXum.Morelli.Coordinates(0,0);
    const str = coordinates.to_string();
    expect(str).toBe('(0,0)');
  });

  test('to string', () => {
    const coordinates = new OpenXum.Morelli.Coordinates(3,2);
    const str = coordinates.to_string();
    expect(str).toBe('(3,2)');

  });

});

describe('Move', () => {
  test ('from', () => {
    const coordinatesA=new OpenXum.Morelli.Coordinates(1,1);
    const coordinatesB=new OpenXum.Morelli.Coordinates(3,5);
    const constructor= new OpenXum.Morelli.Move(1,-1,coordinatesA,coordinatesB);
    const str=constructor.from();
    expect(str).toBe(coordinatesA);

  });

  test ('to', () => {
    const coordinatesA=new OpenXum.Morelli.Coordinates(1,1);
    const coordinatesB=new OpenXum.Morelli.Coordinates(3,5);
    const constructor= new OpenXum.Morelli.Move(1,-1,coordinatesA,coordinatesB);
    const str=constructor.to();
    expect(str).toBe(coordinatesB);

  });

});

describe('Engine', () => {
  test ('currentcolor', () => {
    const constructor = new OpenXum.Morelli.Engine(0,1);
    const str = constructor.current_color();
    expect(str).toBe(1);

  });

  test('get_name', () => {
    const constructor = new OpenXum.Morelli.Engine(0,1);
    const str=constructor.get_name();
    expect(str).toBe('Morelli');

  });

  test ('move', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(12,1);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,0,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,0));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,0,coordinatesA,new OpenXum.Morelli.Coordinates(1,1));
    constructor.move(moveTestA);
    constructor.move(moveTestB);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestC);
    const rep=constructor.get_piece(1,1);
    expect(rep).toBe(0);

  });

    test ('move', () => {
        const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
        const coordinatesA=new OpenXum.Morelli.Coordinates(2,0);
        const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,0,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
        const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,0));
        const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,0,coordinatesA,new OpenXum.Morelli.Coordinates(1,1));
        constructor.move(moveTestA);
        constructor.move(moveTestB);

        constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

        constructor.move(moveTestC);
        const rep=constructor.get_piece(1,1);
        expect(rep).toBe(0);

    });

  test ('move', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,1);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,0,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    constructor.move(moveTestA);
    const rep=constructor.get_piece(12,11);
    expect(rep).toBe(1);

  });

  test ('move', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,1);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,0,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,0));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,0,coordinatesA,new OpenXum.Morelli.Coordinates(1,1));
    constructor.move(moveTestA);
    constructor.move(moveTestB);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestC);
    const rep=constructor.get_piece(6,6);
    expect(rep).toBe(2);
  });

  test ('capture piece', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(1,0));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(2,0));
    const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(1,0),new OpenXum.Morelli.Coordinates(1,1));
    const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(2,0),new OpenXum.Morelli.Coordinates(2,2));
    constructor.move(moveTestA);
    constructor.move(moveTestB);
    constructor.move(moveTestC);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestD);
    constructor.move(moveTestE);
    const rep=constructor.get_piece(1,1);
    expect(rep).toBe(0);
  });

  test ('capture piece', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,3);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(1,0));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(2,0));
    const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(1,0),new OpenXum.Morelli.Coordinates(1,3));
    const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(2,0),new OpenXum.Morelli.Coordinates(2,3));
    constructor.move(moveTestA);
    constructor.move(moveTestB);
    constructor.move(moveTestC);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestD);
    constructor.move(moveTestE);
    const rep=constructor.get_piece(1,3);
    expect(rep).toBe(0);
  });

  test ('get throne', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,12));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(12,10));
    const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(10,0));

    const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,0),new OpenXum.Morelli.Coordinates(2,2));
    const moveTestF=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,12),new OpenXum.Morelli.Coordinates(11,11));

    const moveTestG=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,0),new OpenXum.Morelli.Coordinates(10,2));
    const moveTestH=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(10,0),new OpenXum.Morelli.Coordinates(11,1));


    const moveTestI=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(2,12),new OpenXum.Morelli.Coordinates(2,10));
    const moveTestJ=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,12),new OpenXum.Morelli.Coordinates(1,11));

    const moveTestK=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,10),new OpenXum.Morelli.Coordinates(10,10));
    constructor.move(moveTestA);
    constructor.move(moveTestB);
    constructor.move(moveTestC);
    constructor.move(moveTestD);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestE);
    constructor.move(moveTestF);
    constructor.move(moveTestG);
    constructor.move(moveTestH);
    constructor.move(moveTestI);
    constructor.move(moveTestJ);
    constructor.move(moveTestK);

    const rep=constructor.get_piece(6,6);
    expect(rep).toBe(3);
  });

  test ('get throne', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,12));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(12,10));
    const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(10,0));

    const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,0),new OpenXum.Morelli.Coordinates(2,2));
    const moveTestF=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,12),new OpenXum.Morelli.Coordinates(11,11));

    const moveTestG=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,0),new OpenXum.Morelli.Coordinates(10,2));
    const moveTestH=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(10,0),new OpenXum.Morelli.Coordinates(11,1));

    const moveTestI=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(2,12),new OpenXum.Morelli.Coordinates(2,10));
    const moveTestJ=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,12),new OpenXum.Morelli.Coordinates(1,11));

    const moveTestK=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,10),new OpenXum.Morelli.Coordinates(10,10));
    const moveTestL=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,2),new OpenXum.Morelli.Coordinates(1,1));
    constructor.move(moveTestA);
    constructor.move(moveTestB);
    constructor.move(moveTestC);
    constructor.move(moveTestD);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestE);
    constructor.move(moveTestF);
    constructor.move(moveTestG);
    constructor.move(moveTestH);
    constructor.move(moveTestI);
    constructor.move(moveTestJ);
    constructor.move(moveTestK);
    constructor.move(moveTestL);
    const rep=constructor.get_piece(6,6);
    expect(rep).toBe(4);

  });

  test ('winner', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);
    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,12));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(12,10));
    const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(10,0));

    const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,0),new OpenXum.Morelli.Coordinates(2,2));
    const moveTestF=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,12),new OpenXum.Morelli.Coordinates(11,11));

    const moveTestG=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,0),new OpenXum.Morelli.Coordinates(10,2));
    const moveTestH=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(10,0),new OpenXum.Morelli.Coordinates(11,1));

    const moveTestI=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(2,12),new OpenXum.Morelli.Coordinates(2,10));
    const moveTestJ=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,12),new OpenXum.Morelli.Coordinates(1,11));

    const moveTestK=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,10),new OpenXum.Morelli.Coordinates(10,10));
    const moveTestL=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,2),new OpenXum.Morelli.Coordinates(1,1));

    constructor.move(moveTestA);
    constructor.move(moveTestB);
    constructor.move(moveTestC);
    constructor.move(moveTestD);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestE);
    constructor.move(moveTestF);
    constructor.move(moveTestG);
    constructor.move(moveTestH);
    constructor.move(moveTestI);
    constructor.move(moveTestJ);
    constructor.move(moveTestK);
    constructor.move(moveTestL);
    const rep=constructor.winner_is();
    expect(rep).toBe(-1);

  });

  test ('winner', () => {
    const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
    const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);

    const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
    const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,12),new OpenXum.Morelli.Coordinates(7,7));
    const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,0),new OpenXum.Morelli.Coordinates(5,5));


    constructor.move(moveTestA);

    constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

    constructor.move(moveTestB);
    constructor.move(moveTestC);

    const rep=constructor.winner_is();
    expect(rep).toBe(5);

  });

    test ('winner', () => {
      const constructor = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD,OpenXum.Morelli.Color.BLACK);
      const coordinatesA=new OpenXum.Morelli.Coordinates(0,0);

      const moveTestA=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),coordinatesA);
      const moveTestB=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(12,0));
      const moveTestC=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(7,0));
      const moveTestD=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.PUT_PIECE,-1,new OpenXum.Morelli.Coordinates(-1,-1),new OpenXum.Morelli.Coordinates(0,5));

      const moveTestE=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,0),new OpenXum.Morelli.Coordinates(5,5));
      const moveTestF=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,12),new OpenXum.Morelli.Coordinates(11,11));

      const moveTestG=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,12),new OpenXum.Morelli.Coordinates(5,7));
      const moveTestH=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,0),new OpenXum.Morelli.Coordinates(11,1));

      const moveTestI=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(7,0),new OpenXum.Morelli.Coordinates(7,5));
      const moveTestJ=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(5,12),new OpenXum.Morelli.Coordinates(5,11));

      const moveTestK=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(12,7),new OpenXum.Morelli.Coordinates(7,7));
      const moveTestL=new OpenXum.Morelli.Move(OpenXum.Morelli.Phase.MOVE_PIECE,-1,new OpenXum.Morelli.Coordinates(0,5),new OpenXum.Morelli.Coordinates(1,5));

      constructor.move(moveTestA);
      constructor.move(moveTestB);
      constructor.move(moveTestC);
      constructor.move(moveTestD);

      constructor._phase=OpenXum.Morelli.Phase.MOVE_PIECE;

      constructor.move(moveTestE);
      constructor.move(moveTestF);
      constructor.move(moveTestG);
      constructor.move(moveTestH);
      constructor.move(moveTestI);
      constructor.move(moveTestJ);
      constructor.move(moveTestK);
      constructor.move(moveTestL);


      const rep=constructor.winner_is();
      expect(rep).toBe(3);

    });

});
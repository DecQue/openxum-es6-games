require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;
const AI_MORELLI = require('../../../lib/openxum-ai/games/morelli').default;

let e = new OpenXum.Morelli.Engine(OpenXum.Morelli.GameType.STANDARD, OpenXum.Morelli.Color.BLACK);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Morelli.Color.BLACK, OpenXum.Morelli.Color.WHITE, e);
let p2 = new AI_MORELLI.AlphaBetaPlayer(OpenXum.Morelli.Color.WHITE, OpenXum.Morelli.Color.BLACK, e);
let p = p1;
let moves = [];
let winN=0;
let winB=0;

let nbGame=10;

for(let i = 0; i< nbGame ; i++) {
  console.log("                                     PARTIE N° " + (i+1));
  while (!e.is_finished()) {
    const move = p.move();

    moves.push(move);
    e.move(move);
    if (e.current_color() === OpenXum.Morelli.Color.BLACK) {
      p = p1;
    } else {
      p = p2;
    }
  }

  /*for (let index = 0; index < moves.length; ++index) {
      console.log(moves[index].to_string());
  }*/

  if (e.winner_is() === OpenXum.Morelli.Color.DRAW) {
    console.log("DRAW, NO WINNER!");
  } else {
    console.log("Winner is " + (e.winner_is() === OpenXum.Morelli.Color.KING_BLACK ? 'black' : 'white'));
  }
  if (e.winner_is() === OpenXum.Morelli.Color.KING_BLACK) {
    winN = winN + 1;
  }
  else if (e.winner_is() === OpenXum.Morelli.Color.KING_WHITE) {
    winB = winB + 1;
  }
  e.reset();
}

console.log('Winrate du Black:'+(winN/nbGame)*100+'%');
console.log('Winrate du White:'+(winB/nbGame)*100+'%');

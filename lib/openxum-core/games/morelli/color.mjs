"use strict";

const Color = {NONE: -1, BLACK: 0, WHITE: 1, KING_NONE: 2, KING_BLACK: 3, KING_WHITE: 4, DRAW: 5};

export default Color;

// lib/openxum-core/games/morelli/engine.mjs

import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Move from './move.mjs';
import Coordinates from './coordinates.mjs';
import Phase from './move_type.mjs';
//import les autres classes dont vous avez besoin

class Engine extends OpenXum.Engine {
  constructor(t, c) {
    super();
    this._type = t;
    this._current_color = c;
    this._size = 13;
    this.init_board();
    this._current_king = Color.KING_NONE;
    this._is_game_finished = false;
    this._phase = Phase.PUT_PIECE;
    this._white_pieces = 24;
    this._black_pieces = 24;
    // autres attributs nécessaires à votre jeu
  }

  init_board() {
    this._board = new Array(this._size);
    for (let i = 0; i < this._size; i++)
      this._board[i] = new Array(this._size);
    for (let i = 0; i < this._size; i++)
      for (let j = 0; j < this._size; j++)
        this._board[i][j] = Color.NONE;
    this._board[(this._size - 1) / 2][(this._size - 1) / 2] = Color.KING_NONE;
  }

  reset(){
    this.init_board();
    this._current_king = Color.KING_NONE;
    this._is_game_finished = false;
    this._phase = Phase.PUT_PIECE;
    this._white_pieces = 24;
    this._black_pieces = 24;
  }

  phase(){return this._phase;}

  build_move() {
    return new Move();
  }

  current_color() {
    return this._current_color;
  }

  get_name() {
    return 'Morelli';
  }

  clone() {
    let o = new Engine(this._type, this._color);
    o._size = this._size;
    for (let l = 0; l < this._size; ++l) {
      for (let c = 0; c < this._size; ++c) {
        o._board[l][c] = this._board[l][c];
      }
    }
    return o;
  }


  get_possible_move_list() {
    let list = [];
    if (this._phase === Phase.PUT_PIECE) {
      const free_cells = this._check_place();
      free_cells.forEach((c) => {
        list.push(new Move(Phase.PUT_PIECE, this._current_color, new Coordinates(-1, -1), c));
      });
    }
    else if (this._phase === Phase.MOVE_PIECE) {
      for (let from_ij = 0; from_ij < (this._size * this._size); from_ij++) {
        for (let to_ij = 0; to_ij < (this._size * this._size); to_ij++) {
          let move = new Move(Phase.MOVE_PIECE, this._current_color, new Coordinates(Math.trunc(from_ij / this._size), from_ij % this._size), new Coordinates(Math.trunc(to_ij / this._size), to_ij % this._size));
          if (this._check_coord(move) && (this._check_diag(move) || this._check_ortho(move)) && this._board[move.from().x()][move.from().y()] === this._current_color && to_ij !== from_ij) {
            list.push(move);
          }
        }
      }
    }
    return list;
  }

  nb_white_piece(){ return this._white_pieces; }

  nb_black_piece(){ return this._black_pieces; }

  is_finished() {
    return this._is_game_finished;
  }

  move(move) {
    if(move.phase() === Phase.PUT_PIECE && this._board[move.to().x()][move.to().y()] === Color.NONE
      && (move.to().x() === 0 || move.to().y() === this._size-1 || move.to().x() === this._size-1 || move.to().y() === 0) && this._phase === Phase.PUT_PIECE){
      this._board[move.to().x()][move.to().y()] = this._current_color;
      this._board[this._size-1-move.to().x()][this._size-1-move.to().y()]= (this._current_color===Color.BLACK ?  Color.WHITE : Color.BLACK);
      if(this._check_place().length <= 2) this._current_color=Color.WHITE;
      this._current_color===Color.BLACK ? this._current_color=Color.WHITE : this._current_color=Color.BLACK;
      if(this._check_place().length === 0) this._phase = Phase.MOVE_PIECE;
    }
    else if(this._board[move.from().x()][move.from().y()] === this._current_color && this._check_coord(move) && (this._check_diag(move) || this._check_ortho(move)) &&
    move.phase() === Phase.MOVE_PIECE && !this._is_game_finished && (move.to().x() !== move.from().x() || move.to().y() !== move.from().y()) && this._phase === Phase.MOVE_PIECE){
      this._board[move.to().x()][move.to().y()] = this._board[move.from().x()][move.from().y()];
      this._board[move.from().x()][move.from().y()]=Color.NONE;
      this._capture_piece(move);
      this._get_throne(move.to());
      this._current_color===Color.BLACK ? this._current_color=Color.WHITE : this._current_color=Color.BLACK;
    }
    this._can_play();
  }

  get_piece(i, j){ return this._board[i][j];}


  parse(str) {
      // TODO
  }

  to_string() {
    // TODO
  }

  winner_is() {
    if (this._is_game_finished) {
      if (this._current_king === Color.KING_NONE) return Color.DRAW;
      return this._current_king;
    }
    return Color.NONE;
  }

    //PRIVATE

  _check_ortho(move) {
    if (!((move.from().x() === move.to().x() && move.from().y() !== move.to().y()) || (move.from().x() !== move.to().x() && move.from().y() === move.to().y())))
      return false;
    if (move.from().y() < move.to().y()) {
      for (let j = move.from().y() + 1; j <= move.to().y(); j++)
        if (this._board[move.from().x()][j] !== Color.NONE && this._board[move.from().x()][j] !== Color.KING_NONE) return false;
    }
    else if (move.from().y() > move.to().y()) {
      for (let j = move.to().y(); j < move.from().y(); j++)
        if (this._board[move.from().x()][j] !== Color.NONE && this._board[move.from().x()][j] !== Color.KING_NONE) return false;
    }
    else if (move.from().x() < move.to().x()) {
      for (let i = move.from().x() + 1; i <= move.to().x(); i++)
        if (this._board[i][move.from().y()] !== Color.NONE && this._board[i][move.from().y()] !== Color.KING_NONE) return false;
    }
    else {
      for (let i = move.to().x(); i < move.from().x(); i++)
        if (this._board[i][move.from().y()] !== Color.NONE && this._board[i][move.from().y()] !== Color.KING_NONE) return false;
    }
    return true;
  }

  _check_diag(move) {
    if (!(Math.abs(move.to().y() - move.from().y()) === Math.abs(move.to().x() - move.from().x()))) return false;
    if (move.from().x() <= move.to().x() && move.from().y() <= move.to().y()) {
      for (let ij = 1; ij <= move.to().x() - move.from().x(); ij++)
        if (this._board[move.from().x() + ij][move.from().y() + ij] !== Color.NONE && this._board[move.from().x() + ij][move.from().y() + ij] !== Color.KING_NONE) return false;
    }
    else if (move.from().x() <= move.to().x() && move.from().y() >= move.to().y()) {
      for (let ij = 1; ij <= move.to().x() - move.from().x(); ij++)
        if (this._board[move.from().x() + ij][move.from().y() - ij] !== Color.NONE && this._board[move.from().x() + ij][move.from().y() - ij] !== Color.KING_NONE) return false;
    }
    else if (move.from().x() >= move.to().x() && move.from().y() <= move.to().y()) {
      for (let ij = 1; ij <= move.from().x() - move.to().x(); ij++)
        if (this._board[move.from().x() - ij][move.from().y() + ij] !== Color.NONE && this._board[move.from().x() - ij][move.from().y() + ij] !== Color.KING_NONE) return false;
    }
    else if (move.from().x() >= move.to().x() && move.from().y() >= move.to().y()) {
      for (let ij = 1; ij <= move.from().x() - move.to().x(); ij++)
        if (this._board[move.from().x() - ij][move.from().y() - ij] !== Color.NONE && this._board[move.from().x() - ij][move.from().y() - ij] !== Color.KING_NONE) return false;
    }
    return true;
  }

  _check_coord(move) {
    return (!this._no_get_back(move) && this._board[move.to().x()][move.to().y()] === Color.NONE);
  }

  _no_get_back(move) {
    if (move.from().x() <= move.from().y() && move.from().x() <= this._size - 1 - move.from().y())
      return this._check_move_back(move.from().x(), move.to());
    if (this._size - 1 - move.from().x() <= move.from().y() && this._size - 1 - move.from().x() <= this._size - 1 - move.from().y())
      return this._check_move_back(this._size - 1 - move.from().x(), move.to());
    if (move.from().y() <= move.from().x() && move.from().y() <= this._size - 1 - move.from().x())
      return this._check_move_back(move.from().y(), move.to());
    if (this._size - 1 - move.from().y() <= move.from().x() && this._size - 1 - move.from().y() <= this._size - 1 - move.from().x())
      return this._check_move_back(this._size - 1 - move.from().y(), move.to());
  }


  _check_move_back(xy, to){
    return (to.x() <= xy || to.y() <= xy || to.x() >= this._size - 1 - xy || to.y() >= this._size - 1 - xy);

  }

  _capture_piece(move) {
    let color_piece = (this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK);
    let cpt_piece_get = 0;
    for (let ij = -1; ij < 2; ij += 2) {
      if (move.to().x() + ij > 0 && move.to().x() + ij < this._size - 1
        && this._board[move.to().x() + ij][move.to().y()] === color_piece && this._board[move.to().x() + 2 * ij][move.to().y()] === this._current_color) {
        this._board[move.to().x() + ij][move.to().y()] = this._current_color;
        this._get_throne(new Coordinates(move.to().x() + ij, move.to().y()));
        cpt_piece_get++;
      }
      if (move.to().x() + ij > 0 && move.to().y() + ij > 0 && move.to().x() + ij < this._size - 1 && move.to().y() - ij < this._size - 1
        && this._board[move.to().x() + ij][move.to().y() + ij] === color_piece && this._board[move.to().x() + 2 * ij][move.to().y() + 2 * ij] === this._current_color) {
        this._board[move.to().x() + ij][move.to().y() + ij] = this._current_color;
        this._get_throne(new Coordinates(move.to().x() + ij, move.to().y() + ij));
        cpt_piece_get++;
      }
      if (move.to().y() + ij > 0 && move.to().y() - ij < this._size - 1
        && this._board[move.to().x()][move.to().y() + ij] === color_piece && this._board[move.to().x()][move.to().y() + 2 * ij] === this._current_color) {
        this._board[move.to().x()][move.to().y() + ij] = this._current_color;
        this._get_throne(new Coordinates(move.to().x(), move.to().y() + ij));
        cpt_piece_get++;
      }
      if (move.to().x() + ij > 0 && move.to().y() - ij > 0 && move.to().x() + ij < this._size - 1 && move.to().y() - ij < this._size - 1
        && this._board[move.to().x() + ij][move.to().y() - ij] === color_piece && this._board[move.to().x() + 2 * ij][move.to().y() - 2 * ij] === this._current_color) {
        this._board[move.to().x() + ij][move.to().y() - ij] = this._current_color;
        this._get_throne(new Coordinates(move.to().x() + ij, move.to().y() - ij));
        cpt_piece_get++;
      }
    }
    this._set_nb_picece(cpt_piece_get);
  }

  _set_nb_picece(nb_piece){
    if(this.current_color() === Color.BLACK){
      this._black_pieces += nb_piece;
      this._white_pieces -= nb_piece;
    } else {
      this._black_pieces -= nb_piece;
      this._white_pieces += nb_piece;
    }
  }

  _get_throne(to){
    if((this._current_king === Color.KING_BLACK && this.current_color() === Color.BLACK) || (this._current_king === Color.KING_WHITE && this.current_color() === Color.WHITE)) return;
    if(this._board[to.x()][to.y()]===this.current_color() && this._board[to.y()][this._size-1-to.x()]===this.current_color()
      && this._board[this._size-1-to.y()][to.x()]===this.current_color() && this._board[this._size-1-to.x()][this._size-1-to.y()]===this.current_color()){
      this._current_color===Color.BLACK ? this._board[(this._size-1)/2][(this._size-1)/2] = Color.KING_BLACK : this._board[(this._size-1)/2][(this._size-1)/2] = Color.KING_WHITE;
      this._current_king = this._board[(this._size-1)/2][(this._size-1)/2];
    }
  }

  _check_place(){
    let list = new Array(0);
    for(let i=0; i<this._size; i++){
      for(let j=0; j<this._size; j++){
        if((i===0 || j===0 || i===this._size-1 || j=== this._size-1) && this._board[i][j] === Color.NONE)
          list.push(new Coordinates(i,j));
      }
    }
    return list;
  }

  _can_play(){
    if(this.get_possible_move_list().length === 0)
      this._is_game_finished = true;
  }
}

export default Engine;


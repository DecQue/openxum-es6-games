"use strict";




import Coordinates from './coordinates.mjs';
import Color from './color.mjs';
import Phase from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
  constructor(p, c, f, t) {
    super();
    this._phase = p;
    this._color = c;
    this._from = f;
    this._to = t;

    // + les attributs spécifiques au jeu
  }

// public methods
  decode(str) {
      const to_process = str.split(',');
      if (to_process[0] === "P") {
        this._phase = Phase.PUT_PIECE;
        this._from = new Coordinates(-1, -1);
        this._to = new Coordinates(to_process[2], to_process[3]);
      }
      else if (to_process[0] === "M") {
        this._phase = Phase.MOVE_PIECE;
        this._from = new Coordinates(to_process[2], to_process[3]);
        this._to = new Coordinates(to_process[4], to_process[5]);
      }
      if(to_process[1] === "B") this._color = Color.BLACK;
      else if(to_process[1] === "W") this._color = Color.WHITE;
  }

  encode() {
    if (this._phase === Phase.PUT_PIECE) {
      return "P" + ',' + this._color === Color.BLACK ? 'B' : 'W' + ',' + this._to.x() + ',' + this._to.y();
    }
    else {
      return "M" + ',' + this._color === Color.BLACK ? 'B' : 'W' + ',' + this._from.x() + ',' + this._from.y() + ',' + this._to.x() + ',' + this._to.y();
    }
  }

  from(){
    return this._from;
  }

  to(){
    return this._to;
  }

  color_move(){
    return this._color;
  }

  phase(){
    return this._phase
  }

  from_object(data) {
    this._phase = data.phase();
    this._color = data.color_move();
    this._from = data.from();
    this._to = data.to();
  }

  to_object() {
    return {
      phase: this._phase,
      color: this._color,
      from: this._from,
      to: this._to
    };
  }

  to_string() {
    if (this._phase === Phase.PUT_PIECE) {
      return "put to" + String.fromCharCode('A'.charCodeAt(0) + this._to.x()) + (this._to.y());
    }
    else {
      return "move from " + String.fromCharCode('A'.charCodeAt(0) + this._from.x()) + (this._from.y()) + " to " + String.fromCharCode('A'.charCodeAt(0) + this._to.x()) + (this._to.y());
    }
  }
}

export default Move;


// lib/openxum-core/games/morelli/index.mjs
"use strict";
import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import Phase from './move_type.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';

export default {
  Engine: Engine,
  GameType: GameType,
  Move: Move,
  Phase: Phase,
  Color: Color,
  Coordinates: Coordinates
};

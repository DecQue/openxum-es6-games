"use strict";

import AlphaBetaAI from '../neutreeko/ai_alphabeta.mjs';
import AlphaBetaPlayer from './alphabeta_player.mjs';

export default {
  AlphaBetaPlayer: AlphaBetaPlayer
};

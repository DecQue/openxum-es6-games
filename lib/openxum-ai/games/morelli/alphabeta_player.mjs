"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';
const Morelli = require('../../../openxum-core/games/morelli').default;

class AlphaBetaPlayer extends AlphaBeta {
  constructor(c, o, e) {
    super(c, o, e, 3, 100000);
    this._a_index = c;
    this._o_index = o;
  }

  evaluate(e, depth) {
    // If the game is finished, return final score
    if(e.is_finished()) {
      switch(e.winner_is()) {
        case this.color():
          return this.VICTORY_SCORE;
        case this.opponent_color():
          return -this.VICTORY_SCORE;
        default:
          return 0;
      }
    }

    // Else check if row are connected

    let score = 0;

    if(e.get_piece(6,6)===((e.current_color()===Morelli.Color.BLACK) ? Morelli.Color.KING_BLACK : Morelli.Color.KING_WHITE)) {
      score = 1000;
    } else if (e.get_piece(6,6) !== Morelli.Color.KING_NONE){
      score = - 1000;
    }

    score +=  e.get_possible_move_list().length + (e.current_color()===Morelli.Color.BLACK) ? e.nb_black_piece() : e.nb_white_piece();
    return score;
  }
}

//But :
//Avoir le trône à la fin
//Principe :
//Garder le Maximum de pion possible (avoir le max)
//Déplacement court
//Avoir la plus grande couverture du jeu possible

export default AlphaBetaPlayer;

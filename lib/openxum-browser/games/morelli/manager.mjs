"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Morelli from '../../../openxum-core/games/morelli/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);

        // ...
    }

    build_move() {
        return new Morelli.Move();
    }

    get_current_color() {
        return this._engine.current_color() === Morelli.Color.WHITE ? 'White' : 'Black';
    }

    get_name() {
        return 'morelli';
    }

    get_winner_color() {
        return this.engine().winner_is() === Morelli.Color.KING_WHITE ? 'white' : (this.engine().winner_is()===Morelli.Color.KING_BLACK) ?  'black' : 'draw';
    }

    process_move() { }
}

export default Manager;

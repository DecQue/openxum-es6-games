"use strict";

import Morelli from '../../../openxum-core/games/morelli/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer
        },
        colors: {
            first: Morelli.Color.BLACK,
            init: Morelli.Color.BLACK,
            list: [
                {key: Morelli.Color.BLACK, value: 'black'},
                {key: Morelli.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Morelli.GameType.REGULAR,
            list: [
                {key: Morelli.GameType.BLITZ, value: 'blitz'},
                {key: Morelli.GameType.REGULAR, value: 'regular'}
            ]
        },
        opponent_color(color) {
            return color === Morelli.Color.BLACK ? Morelli.Color.WHITE : Morelli.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};

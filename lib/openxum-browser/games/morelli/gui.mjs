// lib/openxum-browser/games/lyngk/gui.mjs
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';
import Morelli from '../../../openxum-core/games/morelli/index.mjs';

// ...

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this.colors = this._init_color_board();
        this._deltaX = 0;
        this._deltaY = 0;
        this._offsetX = 0;
        this._offsetY = 0;
        this._selected_cell = null;
        this._selected_destination = null;
        this._selected_piece = undefined;
    }

    draw() {
        // fond
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);

        this._context.lineWidth = 10;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#000000";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        this._draw_grid(); // #1
        //this._compute_coordinates(); // #2
        this._draw_state(); // #3
        //this._draw_stack(); // #4
        //this._draw_claimed_colors(); // #5
        //this._draw_possible_move();
    }

    get_move() {
        if (this._selected_cell !== null && this._selected_destination === null) {
            return new Morelli.Move(Morelli.Phase.PUT_PIECE, this._engine.current_color(), {x:-1, y:-1},
                new Morelli.Coordinates(this._selected_cell.x, this._selected_cell.y))
        }
        if (this._selected_cell !== null && this._selected_destination !== null) {
            return new Morelli.Move(Morelli.Phase.MOVE_PIECE, this._engine.current_color(),
                new Morelli.Coordinates(this._selected_cell.x, this._selected_cell.y),
                new Morelli.Coordinates(this._selected_destination.x, this._selected_destination.y))
        }
    }

    is_animate() {
        // Retourne true si le coup provoque des animations
        // (déplacement de pions, par exemple).
        return false;
    }

    is_remote() {
        // Indique si un joueur joue à distance
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    unselect() {
        this._selected_cell = null;
        this._selected_destination = null;
    }

    set_canvas(c) {
        super.set_canvas(c);


        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });


        this._height = this._canvas.height;
        this._width = this._canvas.width;

        this._deltaX = (this._width * 0.95 - 10) / 13;
        this._deltaY = (this._height * 0.95 - 10) / 13;
        this._offsetX = this._width / 2 - this._deltaX * 6.5;
        this._offsetY = this._height / 2 - this._deltaY * 6.5;

        this._scaleX = this._height / this._canvas.offsetHeight;
        this._scaleY = this._width / this._canvas.offsetWidth;

        this.draw();
    }

// private methode

    _draw_state() {

        for (let i = 0; i < 13; ++i) {
            for (let j = 0; j < 13; ++j) {
                const cell = this._engine.get_piece(i, j);

                if (cell === Morelli.Color.WHITE) {
                    Graphics.marble.draw_marble(this._context, this._offsetX + (i + 0.5) * this._deltaX,
                        this._offsetY + (j + 0.5) * this._deltaY, this._deltaX / 1.5, 'white');
                }

                else if (cell === Morelli.Color.BLACK) {
                    Graphics.marble.draw_marble(this._context, this._offsetX + (i + 0.5) * this._deltaX,
                        this._offsetY + (j + 0.5) * this._deltaY, this._deltaX / 1.5, 'black');
                }

                else if(cell === Morelli.Color.KING_BLACK){

                    Graphics.marble.draw_marble(this._context, this._offsetX + (i + 0.5) * this._deltaX,
                        this._offsetY + (j + 0.5) * this._deltaY, this._deltaX/1.1, 'roi');

                    this.is_black_king(i,j);
                }
                else if(cell === Morelli.Color.KING_WHITE){

                    Graphics.marble.draw_marble(this._context, this._offsetX + (i + 0.5) * this._deltaX,
                        this._offsetY + (j + 0.5) * this._deltaY, this._deltaX/1.1, 'roi');

                    this.is_white_king(i,j);
                }

            }
        }
    }

    is_black_king(i,j){

        let radius = (this._deltaX /1.9);
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#000000";
        this._context.beginPath();
        this._context.moveTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY + (radius * 0.2));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.2),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.1));

        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX,  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));

        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.2),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.1));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY + (radius * 0.2));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY+ (radius * 0.2));
        this._context.stroke();
        this._context.fill();


        }


    is_white_king(i,j){

        let radius = (this._deltaX /1.9);
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#ffffff";
        this._context.beginPath();
        this._context.moveTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY + (radius * 0.2));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.2),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.1));

        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX,  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));

        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.2),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.1));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY - (radius * 0.4));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX + (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY + (radius * 0.2));
        this._context.lineTo(this._offsetX + (i + 0.5) * this._deltaX - (radius * 0.4),  this._offsetY + (j + 0.5) * this._deltaY+ (radius * 0.2));
        this._context.stroke();
        this._context.fill();



    }

    _compute_x(x) {
        const index = Math.floor((x - this._offsetX) / this._deltaX);
        const x_ref = this._offsetX + this._deltaX * index;
        const x_ref2 = this._offsetX + this._deltaX * (index + 1);
        let number = -1;

        if (x >= x_ref && x <= x_ref2) {
            number = index + 1;
        }
        return number;
    }

    _compute_y(y) {
        const index = Math.floor((y - this._offsetY) / this._deltaY);
        const y_ref = this._offsetY + this._deltaY * index;
        const y_ref2 = this._offsetY + this._deltaY * (index + 1);
        let number = -1;

        if (y >= y_ref && y <= y_ref2) {
            number = index + 1;
        }
        return number;
    }


    _draw_grid() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        for (let i = 0; i < 13; ++i) {
            for (let j = 0; j < 13; ++j) {
                this._context.fillStyle = this.colors[i][j];
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                this._context.fill();
            }
        }
    }

    _init_color_board() {
        let color = new Array(13);
        for (let i = 0; i < 13; i++)
            color[i] = new Array(13);
        for (let i = 0; i < 13; i++) {
            for (let j = 0; j < 13; j++) {
                if (i === 0 || i === 12 || j === 0 || j === 12) color[i][j] = 'red';
                if (i >= 1 && i <= 11 && j >= 1 && j <= 11) color[i][j] = 'orange';
                if (i >= 2 && i <= 10 && j >= 2 && j <= 10) color[i][j] = 'yellow';
                if (i >= 3 && i <= 9 && j >= 3 && j <= 9) color[i][j] = 'green';
                if (i >= 4 && i <= 8 && j >= 4 && j <= 8) color[i][j] = 'aqua';
                if (i >= 5 && i <= 7 && j >= 5 && j <= 7) color[i][j] = 'blue';
                if (i === 6 && j === 6) color[i][j] = 'silver';
            }
        }

        return color;
    }

    _get_click_position(event) {
        let rect = this._canvas.getBoundingClientRect();
        let x = (event.clientX - rect.left) * this._scaleX;
        let y = (event.clientY - rect.top) * this._scaleY;

        return {x: x, y: y};
    }

    _animate_move() {

            let that = this;
            let deplacement = -1;

            let from = new Morelli.Coordinates(this._selected_cell.x, this._selected_cell.y);
            let to = new Morelli.Coordinates(this._selected_destination.x, this._selected_destination.y);

            if (from.x() < to.x() && from.y() < to.y()) deplacement = 4;
            else if (from.x() > to.x() && from.y() < to.y()) deplacement = 5;
            else if (from.x() > to.x() && from.y() > to.y()) deplacement = 6;
            else if (from.x() < to.x() && from.y() > to.y()) deplacement = 7;
            else if (from.x() < to.x()) deplacement = 0;
            else if (from.x() > to.x()) deplacement = 1;
            else if (from.y() < to.y()) deplacement = 2;
            else if (from.y() > to.y()) deplacement = 3;


            setTimeout(function () {
                that._animate(2, deplacement, from, to);
            }, 50);
        }


    _animate(val, deplacement, from , to) {

        this._engine._board[this._selected_cell.x][this._selected_cell.y]= Morelli.Color.NONE;

        let ptF_x =from.x()*this._deltaX+this._offsetX;
        let ptF_y =from.y()*this._deltaY+this._offsetY;
        let ptT_x =to.x()*this._deltaX+this._offsetX;
        let ptT_y =to.y()*this._deltaY+this._offsetY;

        let newX = ptF_x;
        let newY = ptF_y;
        let continueAnimate = true;


        switch (deplacement) {
            case 0:
                newX = ptF_x + val;
                continueAnimate = newX <= ptT_x;
                break;

            case 1:
                newX = ptF_x - val;
                continueAnimate = newX >= ptT_x;
                break;

            case 2:
                newY = ptF_y + val;
                continueAnimate = newY <= ptT_y;
                break;

            case 3:
                newY = ptF_y - val;
                continueAnimate = newY >= ptT_y;
                break;
            case 4:
                newX = ptF_x + val;
                newY = ptF_y + val;
                continueAnimate = (newY <= ptT_y && newX<=ptT_x);
                break;
            case 5:
                newX = ptF_x - val;
                newY = ptF_y + val;
                continueAnimate = (newY <= ptT_y && newX>=ptT_x);
                break;
            case 6:
                newX = ptF_x - val;
                newY = ptF_y - val;
                continueAnimate = (newY >= ptT_y && newX>=ptT_x);
                break;
            case 7:
                newX = ptF_x + val;
                newY = ptF_y - val;
                continueAnimate = (newY >= ptT_y && newX<=ptT_x);
                break;
            default :
                continueAnimate = false;
                break;
        }

        if (continueAnimate) {
            this.draw();
            this._draw_state();

            if(this._engine.current_color()===Morelli.Color.WHITE){
            Graphics.marble.draw_marble(this._context, newX+this._offsetX, newY+this._offsetY, this._deltaX / 1.5, 'white');}

            if(this._engine.current_color()===Morelli.Color.BLACK){

                Graphics.marble.draw_marble(this._context, newX+this._offsetX, newY+this._offsetY, this._deltaX / 1.5, 'black');}

            let that = this;
            setTimeout(function () {
                that._animate(val + 6, deplacement,from,to);
            }, 10);

        }
        else {
            this.draw();
            this._draw_state();
            let that = this;
            this._engine._board[this._selected_cell.x][this._selected_cell.y]= this._engine.current_color();
            setTimeout(function () {
                that._manager.play();
            }, 50);

        }
    }



    _on_click(event) {
        let pos = this._get_click_position(event);
        const comp_x = this._compute_x(pos.x) - 1;
        const comp_y = this._compute_y(pos.y) - 1;

        if(this._engine.phase() === Morelli.Phase.PUT_PIECE) this._selected_cell=null;

        if (this._selected_cell === null) {
            this._selected_cell = {x: comp_x, y: comp_y};
        } else {
            if (this._selected_destination === null) {
                this._selected_destination = {x: comp_x, y: comp_y};
            }
        }

        if(this._engine.current_color() !== this._engine.get_piece(this._selected_cell.x, this._selected_cell.y) && this._engine.phase() === Morelli.Phase.MOVE_PIECE)
          this._selected_cell=null;

      if(this._selected_destination !== null && this._engine.get_piece(this._selected_destination.x, this._selected_destination.y) !== Morelli.Color.NONE
        && this._engine.phase() === Morelli.Phase.MOVE_PIECE)
        this._selected_destination=null;

        if (this._selected_cell !== null && this._engine.phase() === Morelli.Phase.PUT_PIECE) {
            this._manager.play();

        } else if (this._selected_cell !== null && this._selected_destination !== null && this._engine.phase() === Morelli.Phase.MOVE_PIECE && this._true_coord() && this._king_present())
          this._animate_move();

        }
        //this._animate();

      _true_coord() {
        if (!((!(Math.abs(this._selected_cell.x - this._selected_destination.x) !== Math.abs(this._selected_cell.y - this._selected_destination.y)) || (this._selected_cell.x === this._selected_destination.x
          && this._selected_cell.y !== this._selected_destination.y) || (this._selected_cell.x !== this._selected_destination.x && this._selected_cell.y === this._selected_destination.y)))){
          this._selected_destination=null;
          return false;
        }
        if(this._engine._no_get_back(new Morelli.Move(Morelli.Phase.MOVE_PIECE, this._engine.current_color(), new Morelli.Coordinates(this._selected_cell.x, this._selected_cell.y), new Morelli.Coordinates(this._selected_destination.x, this._selected_destination.y)))){
          this._selected_destination=null;
          return false;
        }
        return true;
      }

      _king_present(){
        if((this._selected_cell.x === this._selected_cell.y && this._selected_destination.x === this._selected_destination.y && this._selected_cell.x &&
          (this._selected_cell.x <6 && this._selected_destination.x >6 || this._selected_cell.x >6 && this._selected_destination.x <6)) ||
          (this._selected_cell.x === this._selected_destination.x && (this._selected_cell.y <6 && this._selected_destination.y >6 || this._selected_cell.y <6 && this._selected_destination.y >6))
          || (this._selected_cell.y === this._selected_destination.y && (this._selected_cell.x <6 && this._selected_destination.x >6 || this._selected_cell.x <6 && this._selected_destination.x >6))){          this._selected_destination=null;
          this._selected_destination=null;
          return false;
        }
        return true;
      }



}

export default Gui;


